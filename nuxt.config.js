export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      {
        charset: 'utf-8',
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1',
      },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico',
      },
    ],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#fff',
  },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['~plugins/vue-inject.js', '~plugins/vue-lazyload.js'],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/eslint-module',
    '@nuxtjs/style-resources',
    [
      'vue-scrollto/nuxt',
      {
        duration: 1000,
        easing: [0.455, 0.03, 0.515, 0.955],
        offset: 0,
        cancelable: false,
      },
    ],
  ],
  styleResources: {
    scss: ['assets/scss/core.scss'],
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      const vueLoader = config.module.rules.find(
        rule => rule.loader === 'vue-loader'
      )
      vueLoader.options.transformAssetUrls = {
        'lazy-image': 'src',
        source: 'srcset',
      }
      const vueRule = config.module.rules.find(rule => rule.test.test('.vue'))
      vueRule.use = [
        {
          loader: vueRule.loader,
          options: vueRule.options,
        },
        {
          loader: 'vue-svg-inline-loader',
        },
      ]
      delete vueRule.loader
      delete vueRule.options
    },
  },
  router: {
    scrollBehavior(to, from, savedPosition) {
      if (savedPosition) {
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            resolve(savedPosition)
          }, 300)
        })
      } else {
        let position = {}
        if (to.matched.length < 2) {
          position = {
            x: 0,
            y: 0,
          }
        } else if (
          to.matched.some(r => r.components.default.options.scrollToTop)
        ) {
          position = {
            x: 0,
            y: 0,
          }
        }

        /*
          YIR.WORKS
        */
        if (from) {
          const pages = process.env.pages
          const works = process.env.works
          const fromName = from.name.replace(/^work-/, '')
          const toName = to.name.replace(/^work-/, '')
          let y = 0
          if (pages.indexOf(fromName) > -1 && pages.indexOf(toName) > -1) {
            y = pages.indexOf(fromName) < pages.indexOf(toName) ? 0 : 99999
          } else if (
            works.indexOf(fromName) > -1 &&
            works.indexOf(toName) > -1
          ) {
            y = works.indexOf(fromName) < works.indexOf(toName) ? 0 : 99999
          }
          position = {
            x: 0,
            y: y,
          }
        }

        if (to.hash) {
          position = {
            selector: to.hash,
          }
        }
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            resolve(position)
          }, 300)
        })
      }
    },
  },
  env: {
    pages: ['index', 'profile', 'works', 'contact'],
    works: ['athome', 'ateditor', 'vpresentation'],
  },
}
