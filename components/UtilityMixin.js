/* eslint-disable */

export default {
  methods: {
    /*
      日付文字列を指定のフォーマットの文字列またはdateオブジェクトに変換
    */
    $_uFormatWPDate(dateString, format = null) {
      const a = dateString.split(/(?:[T\s\/\-\:])/);
      const d = new Date(a[0], a[1] - 1, a[2], a[3], a[4]);
      if (!format) {
        return d;
      }
      return this.$_uFormatDate(d, format);
    },
    /*
      dateオブジェクトを指定のフォーマットの文字列に変換
    */
    $_uFormatDate(date, format, zeroPadding = true) {
      if (!format) format = 'YYYY-MM-DD hh:mm:ss.SSS';
      format = format.replace(/YYYY/g, date.getFullYear());
      if (zeroPadding) {
        format = format.replace(/MM/g, ('0' + (date.getMonth() + 1)).slice(-2));
        format = format.replace(/DD/g, ('0' + date.getDate()).slice(-2));
        format = format.replace(/hh/g, ('0' + date.getHours()).slice(-2));
        format = format.replace(/mm/g, ('0' + date.getMinutes()).slice(-2));
        format = format.replace(/ss/g, ('0' + date.getSeconds()).slice(-2));
      } else {
        format = format.replace(/MM/g, parseInt(('0' + (date.getMonth() + 1)).slice(-2)));
        format = format.replace(/DD/g, parseInt(('0' + date.getDate()).slice(-2)));
        format = format.replace(/hh/g, parseInt(('0' + date.getHours()).slice(-2)));
        format = format.replace(/mm/g, parseInt(('0' + date.getMinutes()).slice(-2)));
        format = format.replace(/ss/g, parseInt(('0' + date.getSeconds()).slice(-2)));
      }
      if (format.match(/S/g)) {
        const milliSeconds = ('00' + date.getMilliseconds()).slice(-3);
        const length = format.match(/S/g).length;
        for (let i = 0; i < length; i++) format = format.replace(/S/, milliSeconds.substring(i, i + 1));
      }
      let days;
      if (this.$translate.lang === 'ja') {
        days = ['日', '月', '火', '水', '木', '金', '土'];
      } else {
        days = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
      }
      format = format.replace(/A/g, days[date.getDay()]);
      return format;
    },

    /*
      テキスト系
    */
    $_uEscapeHtml(string) {
      if (typeof string !== 'string') {
        return '';
      }
      return string.replace(/[&'`"<>]/g, function (match) {
        return {
          '&': '&amp;',
          "'": '&#x27;',
          '`': '&#x60;',
          '"': '&quot;',
          '<': '&lt;',
          '>': '&gt;'
        } [match];
      });
    },
    $_uConvertNewLine(string) {
      let result = string;
      result = result.replace(/\n/g, '<br>');
      result = result.replace(/(\<br\>(\s|\n)*){2,}/g, '<br><br>');
      return result;
    },
    $_uStandardTextFormat(string) {
      let result = this.$_uEscapeHtml(string);
      result = result.replace(/\n/g, '<br>');
      result = result.replace(/(\<br\>(\s|\n)*){2,}/g, '<br><br>');
      result = result.replace(/\[b\](.+?)\[\/b\]/g, '<b class="bold">$1</b>');
      result = result.replace(/((https?|ftp)(:\/\/[-_.!~*'()a-zA-Z0-9;/?:@&=+$,%#]+))/, '<a href="$1" target="_blank">$1</a>');
      return result;
    },
    $_uCommify(number) {
      String.prototype.reverse = function () {
        return this.split('').reverse().join('')
      };
      return number.toString().reverse().replace(/(\d\d\d)(?=\d)(?!\d*\.)/g, '$1,').reverse();
    },

    /*
      色系
    */
    $_uHexToRgb(hex) {
      const res = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
      return res ? [
        parseInt(res[1], 16),
        parseInt(res[2], 16),
        parseInt(res[3], 16)
      ] : null;
    },
    $_uRgbToHex(r, g, b) {
      return "#" + ((1 << 24) + (parseInt(r) << 16) + (parseInt(g) << 8) + parseInt(b)).toString(16).slice(1);
    },
    $_uHslToRgb(h, s, l) {
      var r, g, b;
      if (s == 0) {
        r = g = b = l; // achromatic
      } else {
        function hue2rgb(p, q, t) {
          if (t < 0) t += 1;
          if (t > 1) t -= 1;
          if (t < 1 / 6) return p + (q - p) * 6 * t;
          if (t < 1 / 2) return q;
          if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
          return p;
        }
        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q;
        r = hue2rgb(p, q, h + 1 / 3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1 / 3);
      }
      return [parseInt(r * 255), parseInt(g * 255), parseInt(b * 255)];
    },
    $_uRgbToHsl(r, g, b) {
      r /= 255, g /= 255, b /= 255;
      var max = Math.max(r, g, b),
        min = Math.min(r, g, b);
      var h, s, l = (max + min) / 2;
      if (max == min) {
        h = s = 0; // achromatic
      } else {
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
        switch (max) {
          case r:
            h = (g - b) / d + (g < b ? 6 : 0);
            break;
          case g:
            h = (b - r) / d + 2;
            break;
          case b:
            h = (r - g) / d + 4;
            break;
        }
        h /= 6;
      }
      return [h, s, l];
    },
    $_uDarken(p, r, g, b, a = 1) {
      const hsl = this.$_uRgbToHsl(r, g, b);
      hsl[2] -= p;
      return this.$_uHslToRgb(hsl[0], hsl[1], hsl[2]);
    },
    $_uContrastColor(hex, defaultColor = [255, 255, 255]) {
      if (this.$_uLuminance(this.$_uHexToRgb(hex)) > 200) {
        const rgb = this.$_uHexToRgb(hex);
        const hsl = this.$_uRgbToHsl(rgb[0], rgb[1], rgb[2]);
        hsl[1] -= 0.1;
        hsl[2] -= 0.4;
        return this.$_uHslToRgb(hsl[0], hsl[1], hsl[2]);
      } else {
        return defaultColor;
      }
    },
    $_uLuminance(rgba) {
      const r = 0.298912;
      const g = 0.586611;
      const b = 0.114478;
      return Math.floor(r * rgba[0] + g * rgba[1] + b * rgba[2]);
    },
    /*
      その他
    */
    $_uCopyObj(obj) {
      let result;
      if (Array.isArray(obj)) {
        result = [].concat(obj);
      } else if (typeof obj === 'object') {
        result = Object.assign({}, obj);
      } else {
        result = obj;
      }
      return result;
    }
  }
};
