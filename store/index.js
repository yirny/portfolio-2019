import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const state = () => ({
  panelId: '',
  panelColors: [],
  panelImage: '',
  gradient: [],
  lang: 'en',
})

export const mutations = {
  setPanelId(state, id) {
    state.panelid = id
  },
  setPanelColors(state, color) {
    state.panelColors = color
  },
  setPanelImage(state, imgUrl) {
    state.panelImage = imgUrl
  },
  setGradient(state, colors) {
    state.gradient = colors
  },
  setLang(state, lang) {
    state.lang = lang
  },
}

export const actions = {
  /* --------------------------------
    パネルアニメーション
  -------------------------------- */
  changePanel(context, panel) {
    const pid = panel.id // こうしないと意図しないタイミングで変化する？
    context.commit('setPanelId', pid)
    if (panel.gradient) context.commit('setGradient', panel.gradient)
    if (panel.panelColors) context.commit('setPanelColors', panel.panelColors)
    if (panel.panelImage) context.commit('setPanelImage', panel.panelImage)
    // 共通CSS変数の変更
    if (panel.colors) {
      const delay = panel.colorTransitionDelay || 1000
      setTimeout(() => {
        const app = document.getElementById('app')
        app.style.setProperty(`--color--primary`, panel.colors.primary)
        app.style.setProperty(`--color--text`, panel.colors.text)
        app.style.setProperty(`--color--primary-on`, panel.colors.primaryOn)
      }, delay)
    }
  },
}
